const inputteks = document.querySelector(".text");
const inputdate = document.querySelector(".date");
const input = document.querySelector(".add");
const ul = document.querySelector(".task");
// const empty = document.querySelector("empty");

input.addEventListener("click", (e)=>{
    e.preventDefault();
    // console.log(1);

    const huruf = inputteks.value;

    if(!huruf){
      alert("isi dengan lengkap");
      return;
    }
    const tanggal = inputdate.value;
    if(!tanggal){
      alert("isi dengan lengkap");
      return;
    }
    
    const dive = document.createElement('tr');
    dive.classList.add('k');


  //   dive.innerHTML =`   
  //   <input readonly="readonly" type="text" id="text" class="form-control text" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="button-addon2" value="${huruf}">
  //   <input readonly="readonly" type="date" id="date" class="form-control date" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="button-addon2" value=${tanggal}>
  // `;
  const task_input_text = document.createElement('input');
		task_input_text.classList.add('text');
    task_input_text.classList.add('form-control');
		task_input_text.type = 'text';
		task_input_text.value = huruf;
    task_input_text.setAttribute('required','');
		task_input_text.setAttribute('readonly', 'readonly');
 
  const task_input_date = document.createElement('input');
		task_input_date.classList.add('date');
    task_input_date.classList.add('form-control');
		task_input_date.type = 'date';
		task_input_date.value = tanggal;
    task_input_date.setAttribute('required','');
		task_input_date.setAttribute('readonly', 'readonly');




  const task_edit_el = document.createElement('button');
  task_edit_el.setAttribute("class", "democlass");
  task_edit_el.type ='button';
  task_edit_el.classList.add('edit');
  task_edit_el.classList.add('btn');
  task_edit_el.classList.add('btn-outline-secondary');
  task_edit_el.innerText = 'Edit';
  

  const task_delete_el = document.createElement('button');
  task_delete_el.setAttribute("type", "button");
  task_delete_el.type ='button';
  task_delete_el.classList.add('delete');
  task_delete_el.classList.add('btn');
  task_delete_el.classList.add('btn-outline-secondary');
  task_delete_el.innerText = 'delete';

  const table_input_text= document.createElement('th');
  table_input_text.classList.add('row');
  const table_input_date= document.createElement('td');
  const table_delete= document.createElement('td');
  const table_edit= document.createElement('td');

  table_input_text.appendChild(task_input_text);
  table_input_date.appendChild(task_input_date);
  table_edit.appendChild(task_edit_el);
  table_delete.appendChild(task_delete_el);



  dive.appendChild(table_input_text);
  dive.appendChild(table_input_date);
  dive.appendChild(table_edit);
	dive.appendChild(table_delete);


 ul.appendChild(dive);

  task_edit_el.addEventListener('click', (e) => {
    if (task_edit_el.innerText.toLowerCase() == "edit") {
      task_edit_el.innerText = "Save";
      task_input_text.removeAttribute("readonly");
      task_input_date.removeAttribute("readonly");
      task_input_text.focus();
      task_input_date.focus();
    }
     else {
      task_edit_el.innerText = "Edit";
      task_input_text.setAttribute("readonly", "readonly");
      task_input_date.setAttribute("readonly", "readonly");

    }
  });


task_delete_el.addEventListener('click', (e) => {
  ul.removeChild(dive);
});
 
  


    
});